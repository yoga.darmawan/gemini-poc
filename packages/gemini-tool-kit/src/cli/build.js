/* eslint-disable import/no-dynamic-require */
const commonjs = require("@rollup/plugin-commonjs");
const resolve = require("@rollup/plugin-node-resolve").default;
const path = require("path");
const rollup = require("rollup");
const typescript = require("rollup-plugin-typescript2");
const peerDepsExternal = require("rollup-plugin-peer-deps-external");

const cwd = process.cwd();
const packageJson = require(path.join(cwd, "package.json"));

const inputOptions = {
  input: packageJson.src,
  plugins: [
    peerDepsExternal(),
    resolve(),
    commonjs(),
    typescript({ useTsconfigDeclarationDir: true }),
  ],
  external: [
    "react",
    "react-dom",
    "@emotion/react",
    "@emotion/styled",
    "@mui/material",
    "@mui/icons-material",
  ],
};

const outputOptions = [
  {
    file: packageJson.main,
    format: "cjs",
    sourcemap: true,
  },
  {
    file: packageJson.module,
    format: "esm",
    sourcemap: true,
  },
];

async function build() {
  // create bundle
  const bundle = await rollup.rollup(inputOptions);
  // loop through the options and write individual bundles
  outputOptions.forEach(async (options) => {
    await bundle.write(options);
  });
}

build();
