#!/usr/bin/env node
/* eslint-disable global-require */
/* eslint-disable no-unused-expressions */

const yargs = require("yargs");

yargs
  .command(
    "build",
    "Build utils library library",
    () => {},
    () => {
      require("./cli/build");
    }
  )
  .demandCommand(1, "Please choose your command")
  .epilog("Tool Kit CLI")
  .help()
  .strict().argv;
