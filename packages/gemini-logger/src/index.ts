import { logger } from "./logger";
import type { ILogData, ILogPlatform } from "./types";

export { logger, ILogData, ILogPlatform };
