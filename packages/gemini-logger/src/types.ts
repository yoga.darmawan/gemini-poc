export interface ILogPlatform {
  console: ILogger;
  ddrum: ILogger;
  // gcp: () => any;
}

export interface ILogData {
  _cID: string;
  eventName: string;
  level: string;
  ts: Date;
  data: any;
}

export interface ILogger {
  trace(data: any): void;
  debug(data: any): void;
  info(data: any): void;
  warn(data: any): void;
  error(data: any): void;
  fatal(data: any): void;
  loadingTime(data: string, timming?: number): void;
  userContext(data: any): void;
}
