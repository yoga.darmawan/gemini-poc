import { ConsoleAdapter, DatadogRumAdapter } from "./lib";
import type { ILogPlatform } from "./types";

export const is_server = () => {
  return !(typeof window != "undefined" && window.document);
};

export const logger: ILogPlatform = {
  console: new ConsoleAdapter(),
  ddrum: new DatadogRumAdapter(),
};
