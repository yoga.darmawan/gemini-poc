import ConsoleAdapter from "./console";
import DatadogRumAdapter from "./ddrum";

export { ConsoleAdapter, DatadogRumAdapter };
