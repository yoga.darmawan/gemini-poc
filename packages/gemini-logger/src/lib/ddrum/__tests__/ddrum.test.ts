import DatadogRumAdapter from "../index";
import { datadogRum } from "@datadog/browser-rum";
import { ILogData } from "src/types";

const eventNameMock = "this is logging data";
const mockEventData = {
  _cID: "",
  eventName: "",
  level: "level",
  ts: new Date(),
  data: {},
};
const eventTimingMock = 10;

describe("DDRum Logger Test Groups", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  // 1. Test DDRum Trace Data String
  it("1. Test DDRum Trace Data String", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const trace = jest
      .spyOn(datadogRum, "addAction")
      .mockImplementation((name, data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.trace(eventNameMock);
    expect(trace).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });
  // 2. Test DDRum Trace Data Object
  it("2. Test DDRum Trace Data Object", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const trace = jest
      .spyOn(datadogRum, "addAction")
      .mockImplementation((name, data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.trace({ eventName: eventNameMock });
    expect(trace).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });

  // 3. Test DDRum Debug Data String
  it("3. Test DDRum Debug Data String", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const debug = jest
      .spyOn(datadogRum, "addAction")
      .mockImplementation((name, data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.debug(eventNameMock);
    expect(debug).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });
  // 4. Test DDRum Debug Data Object
  it("4. Test DDRum Debug Data Object", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const debug = jest
      .spyOn(datadogRum, "addAction")
      .mockImplementation((name, data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.debug({ eventName: eventNameMock });
    expect(debug).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });

  // 5. Test DDRum Info Data String
  it("5. Test DDRum Info Data String", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const info = jest
      .spyOn(datadogRum, "addAction")
      .mockImplementation((name, data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.info(eventNameMock);
    expect(info).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });
  // 6. Test DDRum Info Data Object
  it("6. Test DDRum Info Data Object", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const info = jest
      .spyOn(datadogRum, "addAction")
      .mockImplementation((name, data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.info({ eventName: eventNameMock });
    expect(info).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });

  // 7. Test DDRum Warn Data String
  it("7. Test DDRum Warn Data String", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const warn = jest
      .spyOn(datadogRum, "addAction")
      .mockImplementation((name, data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.warn(eventNameMock);
    expect(warn).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });
  // 8. Test DDRum Warn Data Object
  it("8. Test DDRum Warn Data Object", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const warn = jest
      .spyOn(datadogRum, "addAction")
      .mockImplementation((name, data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.warn({ eventName: eventNameMock });
    expect(warn).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });

  // 9. Test DDRum Error Data String
  it("9. Test DDRum Error Data String", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const error = jest
      .spyOn(datadogRum, "addError")
      .mockImplementation((data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.error(eventNameMock);
    expect(error).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });
  // 10. Test DDRum Error Data Object
  it("10. Test DDRum Error Data Object", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const error = jest
      .spyOn(datadogRum, "addError")
      .mockImplementation((data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.error(eventNameMock);
    expect(error).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });

  // 11. Test DDRum Fatal Data String
  it("11. Test DDRum Fatal Data String", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const fatal = jest
      .spyOn(datadogRum, "addError")
      .mockImplementation((data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.fatal(eventNameMock);
    expect(fatal).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });
  // 12. Test DDRum Fatal Data Object
  it("12. Test DDRum Fatal Data Object", async () => {
    let eventData: ILogData = mockEventData;
    const log = new DatadogRumAdapter();
    const fatal = jest
      .spyOn(datadogRum, "addError")
      .mockImplementation((data) => {
        if (data) {
          eventData = data as ILogData;
        }
      });
    log.fatal(eventNameMock);
    expect(fatal).toHaveBeenCalled();
    expect(eventData.eventName).toBe(eventNameMock);
  });

  // 13. Test Loading Time DDRum
  it("13. Test Loading Time DDRum", async () => {
    let eventName;
    let eventTiming;
    const log = new DatadogRumAdapter();
    const consoleLog = jest
      .spyOn(datadogRum, "addTiming")
      .mockImplementation((name, timing) => {
        eventName = name;
        eventTiming = timing;
      });
    log.loadingTime(eventNameMock);
    expect(consoleLog).toHaveBeenCalled();
    log.loadingTime(eventNameMock, eventTimingMock);
    expect(eventName).toBe(eventNameMock);
    expect(eventTiming).toBe(eventTimingMock);
  });

  // 14. Test User Context DDRum
  it("14. Test User Context DDRum", async () => {
    const newEventNameMock = "ini nama event terbaru";
    let eventName;
    const log = new DatadogRumAdapter();
    const consoleLog = jest
      .spyOn(datadogRum, "setUser")
      .mockImplementation((name) => {
        eventName = name;
      });
    log.userContext(eventNameMock);
    expect(consoleLog).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
    log.userContext(newEventNameMock);
    expect(eventName).toBe(newEventNameMock);
  });
});
