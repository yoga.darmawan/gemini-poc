import { datadogRum } from "@datadog/browser-rum";

import { ILogger, ILogData } from "../../types";
import { level } from "../../const";

class DatadogRumAdapter implements ILogger {
  #logData: ILogData;

  constructor() {
    this.#logData = {
      _cID: "",
      ts: new Date(),
      level: level.INFO,
      eventName: "",
      data: {},
    };
  }

  transformData(oriData: any) {
    const context = datadogRum.getInternalContext();
    const { level, eventName, ...restData } = oriData;
    this.#logData = {
      _cID: context?.session_id!,
      ts: new Date(),
      level: level,
      eventName: eventName,
      data: restData,
    };
  }

  public trace(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.TRACE;
    this.transformData(data);
    return datadogRum.addAction(this.#logData.eventName, this.#logData);
  }

  public debug(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.DEBUG;
    this.transformData(data);
    return datadogRum.addAction(this.#logData.eventName, this.#logData);
  }

  public info(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.INFO;
    this.transformData(data);
    return datadogRum.addAction(this.#logData.eventName, this.#logData);
  }

  public warn(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.WARN;
    this.transformData(data);
    return datadogRum.addAction(this.#logData.eventName, this.#logData);
  }

  public error(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.ERROR;
    this.transformData(data);
    return datadogRum.addError(this.#logData);
  }

  public fatal(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.FATAL;
    this.transformData(data);
    return datadogRum.addError(this.#logData);
  }

  public loadingTime(name: string, timming?: number) {
    if (timming) {
      return datadogRum.addTiming(name, timming);
    }
    return datadogRum.addTiming(name);
  }

  public userContext(data: any) {
    return datadogRum.setUser(data);
  }
}

export default DatadogRumAdapter;
