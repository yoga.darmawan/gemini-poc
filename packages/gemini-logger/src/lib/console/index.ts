import { ILogger, ILogData } from "../../types";
import { level } from "../../const";

class ConsoleAdapter implements ILogger {
  #logData: ILogData;

  constructor() {
    this.#logData = {
      _cID: "",
      ts: new Date(),
      level: level.INFO,
      eventName: "",
      data: {},
    };
  }

  transformData(oriData: any) {
    const { level, eventName, ...restData } = oriData;
    this.#logData = {
      _cID: "",
      ts: new Date(),
      level: level,
      eventName: eventName,
      data: restData,
    };
  }

  public trace(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.TRACE;
    this.transformData(data);
    return console.trace(this.#logData);
  }

  public debug(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.DEBUG;
    this.transformData(data);
    return console.debug(this.#logData);
  }

  public info(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.INFO;
    this.transformData(data);
    return console.info(this.#logData);
  }

  public warn(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.WARN;
    this.transformData(data);
    return console.warn(this.#logData);
  }

  public error(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.ERROR;
    this.transformData(data);
    return console.error(this.#logData);
  }

  public fatal(data: any) {
    if (typeof data === "string") {
      data = { eventName: data };
    }
    data.level = level.FATAL;
    this.transformData(data);
    return console.error(this.#logData);
  }

  public loadingTime(name: string, timming?: number) {
    if (timming) {
      return console.log(name, timming);
    }
    return console.log(name);
  }

  public userContext(data: any) {
    return console.log(data);
  }
}

export default ConsoleAdapter;
