import ConsoleAdapter from "../index";

const eventNameMock = "this is logging data";
const eventTimingMock = 10;

describe("Console Logger Test Groups", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  // 1. Test Console Trace Data String
  it("1. Test Console Trace Data String", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const trace = jest.spyOn(console, "trace").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.trace(eventNameMock);
    expect(trace).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });
  // 2. Test Console Trace Data Object
  it("2. Test Console Trace Data Object", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const trace = jest.spyOn(console, "trace").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.trace({ eventName: eventNameMock });
    expect(trace).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });

  // 3. Test Console Debug Data String
  it("3. Test Console Debug Data String", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const debug = jest.spyOn(console, "debug").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.debug(eventNameMock);
    expect(debug).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });
  // 4. Test Console Debug Data Object
  it("4. Test Console Debug Data Object", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const debug = jest.spyOn(console, "debug").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.debug({ eventName: eventNameMock });
    expect(debug).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });

  // 5. Test Console Info Data String
  it("5. Test Console Info Data String", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const info = jest.spyOn(console, "info").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.info(eventNameMock);
    expect(info).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });
  // 6. Test Console Info Data Object
  it("6. Test Console Info Data Object", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const info = jest.spyOn(console, "info").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.info({ eventName: eventNameMock });
    expect(info).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });

  // 7. Test Console Warn Data String
  it("7. Test Console Warn Data String", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const warn = jest.spyOn(console, "warn").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.warn(eventNameMock);
    expect(warn).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });
  // 8. Test Console Warn Data Object
  it("8. Test Console Warn Data Object", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const warn = jest.spyOn(console, "warn").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.warn({ eventName: eventNameMock });
    expect(warn).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });

  // 9. Test Console Error Data String
  it("9. Test Console Error Data String", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const error = jest.spyOn(console, "error").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.error(eventNameMock);
    expect(error).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });
  // 10. Test Console Error Data Object
  it("10. Test Console Error Data Object", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const error = jest.spyOn(console, "error").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.error({ eventName: eventNameMock });
    expect(error).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });

  // 11. Test Console Fatal Data String
  it("11. Test Console Fatal Data String", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const error = jest.spyOn(console, "error").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.fatal(eventNameMock);
    expect(error).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });
  // 12. Test Console Fatal Data Object
  it("12. Test Console Fatal Data Object", async () => {
    let eventName;
    const log = new ConsoleAdapter();
    const error = jest.spyOn(console, "error").mockImplementation((data) => {
      eventName = data.eventName;
    });
    log.fatal({ eventName: eventNameMock });
    expect(error).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
  });

  // 13. Test Loading Time Console
  it("13. Test Loading Time Console", async () => {
    let eventName;
    let eventTiming;
    const log = new ConsoleAdapter();
    const consoleLog = jest
      .spyOn(console, "log")
      .mockImplementation((name, timing) => {
        eventName = name;
        eventTiming = timing;
      });
    log.loadingTime(eventNameMock);
    expect(consoleLog).toHaveBeenCalled();
    log.loadingTime(eventNameMock, eventTimingMock);
    expect(eventName).toBe(eventNameMock);
    expect(eventTiming).toBe(eventTimingMock);
  });

  // 14. Test User Context Console
  it("14. Test User Context Console", async () => {
    const newEventNameMock = "ini nama event terbaru";
    let eventName;
    const log = new ConsoleAdapter();
    const consoleLog = jest.spyOn(console, "log").mockImplementation((name) => {
      eventName = name;
    });
    log.userContext(eventNameMock);
    expect(consoleLog).toHaveBeenCalled();
    expect(eventName).toBe(eventNameMock);
    log.userContext(newEventNameMock);
    expect(eventName).toBe(newEventNameMock);
  });
});
