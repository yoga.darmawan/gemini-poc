/*
This is the Fetcher Helper to facilitate FetcherAdapter initial config
Built in logging feature to log your request-response data
*/

// import { logger } from '@/shared/utils/common';

// Standardize your handler when your API call success get response
// Params:
// - response: the original response from the API call
// - context: another data for certain purpose. ex: logging
const clientSuccessHandler = (response: any, context?: any) => {
  // logger.info({ ...response, ...context });
  return response;
};

// Standardize your handler when your API call failed get response
// Params:
// - error: the original error from the API call
// - context: another data for certain purpose. ex: logging
const clientErrorHandler = (error: any, context?: any) => {
  // logger.error({ ...error, ...context });
  return error;
};

// Standardize your handler when your API call failed get response
// Params:
// - url: path url to be added with query param
// - obj: object to be translated to url query param
// Example: {name: test, email: test@test.com} => ?name=test&email=test@test.com
const createUrlParam = (url = "", obj: any) => {
  if (obj && Object.keys(obj).length > 0) {
    const encode: any = (obj: any, nesting = "") => {
      const pairs = Object.entries(obj).map(([key, value]) => {
        if (typeof value === "object") {
          return encode(value, `${nesting}${encodeURIComponent(key)}=`);
        }
        return [
          nesting + encodeURIComponent(key),
          encodeURIComponent(`${value}`),
        ].join("=");
      });

      return pairs.join("&");
    };

    const query = encode(obj);
    if (url.includes("?")) {
      return `${url}&${query}`;
    }
    return `${url}?${query}`;
  }

  return url;
};

export { createUrlParam, clientErrorHandler, clientSuccessHandler };
