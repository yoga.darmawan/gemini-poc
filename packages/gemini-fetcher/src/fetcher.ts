/*
This is Fetcher Adapter.
You can use any fetching library like axios, fetch, superagent, etc as long as it implement the BaseFetch standard interface
Also you can implement success & error handler according to your need
Note:
- When you already implement this adapte for axios and on the feature you should change from axios to superagent, 
  just create superagent implmentation & change fetcher instance to superagent without have to change all the repository code
*/

import { createAxios } from "./lib/axios/helper";
import {
  createUrlParam,
  clientSuccessHandler,
  clientErrorHandler,
} from "./helper";
import { IBaseFetcher } from "./types";

export class FetcherAdapter {
  #id: string;
  #url: string;
  #queryParam: any;
  #data: any;
  _fetcher: IBaseFetcher;
  #context: any;
  #successHandler: any;
  #errorHandler: any;

  constructor() {
    this.#id = "";
    this.#url = "";
    this.#queryParam = {};
    this.#data = {};
    this._fetcher = createAxios({ baseURL: "" }).instance;
    this.#context = {};
    this.#successHandler = (resp: any, ctx: any) =>
      clientSuccessHandler(resp, ctx);
    this.#errorHandler = (err: any, ctx: any) => clientErrorHandler(err, ctx);
  }

  // Getter & Setter
  setFetcher(fetcher: any) {
    this._fetcher = fetcher;
    return this;
  }

  setContext(ctx: any) {
    this.#context = ctx;
    return this;
  }

  setId(id: string) {
    this.#id = id;
    return this;
  }

  setUrl(url: string) {
    this.#url = url;
    return this;
  }

  setQueryParam(queryParam: any) {
    this.#queryParam = queryParam;
    return this;
  }

  setData(data: any) {
    this.#data = data;
    return this;
  }

  setSuccessHandler(successHandler: any) {
    this.#successHandler = successHandler;
    return this;
  }

  setErrorHandler(errorHandler: any) {
    this.#errorHandler = errorHandler;
    return this;
  }

  // Request Processor
  async post() {
    try {
      const response: any = await this._fetcher.post(
        createUrlParam(this.#url, this.#queryParam),
        this.#data
      );
      return this.#successHandler(response, this.#context);
    } catch (error) {
      throw this.#errorHandler(error, this.#context);
    }
  }

  async get() {
    try {
      const response: any = await this._fetcher.get(
        createUrlParam(this.#url, this.#queryParam)
      );
      return this.#successHandler(response, this.#context);
    } catch (error: any) {
      throw this.#errorHandler(error, this.#context);
    }
  }

  async getById() {
    try {
      const url: string = `${this.#url}/${this.#id}`;
      const response: any = await this._fetcher.get(
        createUrlParam(url, this.#queryParam)
      );
      return this.#successHandler(response, this.#context);
    } catch (error) {
      throw this.#errorHandler(error, this.#context);
    }
  }

  async putById() {
    try {
      const url: string = `${this.#url}/${this.#id}`;
      const response: any = await this._fetcher.put(url, this.#data);
      return this.#successHandler(response, this.#context);
    } catch (error) {
      throw this.#errorHandler(error, this.#context);
    }
  }

  async patchById() {
    try {
      const url: string = `${this.#url}/${this.#id}`;
      const response: any = await this._fetcher.patch(url, this.#data);
      return this.#successHandler(response, this.#context);
    } catch (error) {
      throw this.#errorHandler(error, this.#context);
    }
  }

  async deleteById() {
    try {
      const url: string = `${this.#url}/${this.#id}`;
      const response: any = await this._fetcher.delete(url, {});
      return this.#successHandler(response, this.#context);
    } catch (error) {
      throw this.#errorHandler(error, this.#context);
    }
  }
}
