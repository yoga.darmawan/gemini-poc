/*
In Axios Builder Given functionality to 
- Create instance, 
- Create interceptor on request & response with logging feature to log your request & response data
Note:
- In this builder only pass log-data to logger package. 
- How the log-data transformed & sent to logger server is on the logger package logic.
*/

// import { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

// import { requestInterceptor, responseInterceptor } from "./interceptor";
import { ICreation } from "./types";
import { AxiosFetcher } from "./fetcher";

// Instance creator
const createAxios = (data: ICreation) => {
  const axios = new AxiosFetcher();
  axios.setBaseURL(data.baseURL);
  if (data.auth) {
    axios.setHeaders({
      Authorization: data.auth,
    });
  }
  axios.build();

  axios.setRequestInterceptor(data.requestInterceptor);
  axios.setResponseInterceptor(data.responseInterceptor);

  return axios;
};

// const maskingConfig = (config: AxiosRequestConfig) => {
//   try {
//     let data = null;
//     if (typeof config?.data === "string") {
//       data = JSON.parse(config?.data);
//     } else if (typeof config?.data === "object") {
//       data = config?.data;
//     }
//     if (data && data.passcode) {
//       data.passcode = "redacted";
//     }
//     config.data = JSON.stringify(data);

//     const headers = config?.headers;
//     if (headers?.Authorization) {
//       headers.Authorization = headers.Authorization ? "redacted" : "undefined";
//     } else if (headers?.authorization) {
//       headers.authorization = headers.authorization ? "redacted" : "undefined";
//     }
//     config.headers = headers;

//     return config;
//   } catch (error) {
//     throw error;
//   }
// };

// const maskingError = (error: AxiosError) => {
//   try {
//     const config: AxiosRequestConfig = !error?.response
//       ? error?.config
//       : error?.response.config;
//     const maskedConfig = maskingConfig(config);
//     if (!error?.response) {
//       error.config = maskedConfig;
//     } else if (error?.response) {
//       error.response.config = maskedConfig;
//     }

//     return error;
//   } catch (error) {
//     throw error;
//   }
// };

// const maskingRequestConfig = (err: AxiosResponse & AxiosError) => {
//   try {
//     const error = err;
//     // SEARCH FOR CONFIG OBJECT BASE ON ERROR RESPONSE TYPE
//     const config: AxiosRequestConfig = !error?.response
//       ? error?.config
//       : error?.response.config;
//     // COPY DATA BASED ON TYPE
//     let data = null;
//     if (typeof config?.data === "string") {
//       data = JSON.parse(config?.data);
//     } else if (typeof config?.data === "object") {
//       data = config?.data;
//     }
//     // MODIFY DATA
//     if (data) {
//       data.passcode = data?.passcode ? "redacted" : "undefined";
//     }
//     // REASSIGN DATA BACK TO CONFIG
//     config.data = JSON.stringify(data);
//     // COPY HEADERS
//     const headers = config?.headers;
//     // MODIFY HEADERS
//     if (headers?.Authorization) {
//       headers.Authorization = headers.Authorization ? "redacted" : "undefined";
//     } else if (headers?.authorization) {
//       headers.authorization = headers.authorization ? "redacted" : "undefined";
//     }
//     // REASSIGN HEADERS BACK TO CONFIG
//     config.headers = headers;
//     // REASSIGN CONFIG BACK TO ERROR RESPONSE
//     if (!error?.response) {
//       error.config = config;
//     } else if (error?.response) {
//       error.response.config = config;
//     }
//     // RETUR ERROR
//     return error;
//   } catch (error) {
//     throw error;
//   }
// };

// const clientErrorHandler = (err: any): IErrorResponse => {
//   try {
//     const maskedErr = maskingRequestConfig(err);
//     if (!maskedErr?.response) {
//       return maskedErr;
//     }
//     const { data, status, config }: AxiosResponse = maskedErr.response;
//     if (data?.config) {
//       return data;
//     }
//     const newData =
//       data && typeof data === "object" ? data : { error_code: data };
//     const response = {
//       config,
//       status,
//       data: newData,
//     };
//     return response;
//   } catch (error) {
//     throw error;
//   }
// };

export {
  createAxios,
  // clientErrorHandler,
  // requestInterceptor,
  // responseInterceptor,
  // maskingError,
  // maskingConfig,
};
