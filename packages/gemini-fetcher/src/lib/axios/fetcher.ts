/*
This is AxiosFetch definition. The Request Processor (get, post, etc) already implement according to BaseFetch standard
Note:
- You can implment another library like fetch/superagent with this implementation example with their specific features
*/

import axios, { AxiosRequestHeaders } from "axios";

import { IBaseFetcher } from "../../types";
import { IRequestInterceptor, IResponseInterceptor } from "./types";

export class AxiosFetcher implements IBaseFetcher {
  #baseURL: string;
  #headers: AxiosRequestHeaders;
  #requestInterceptor?: IRequestInterceptor;
  #responseInterceptor?: IResponseInterceptor;
  #timeout: number;
  instance: any;

  constructor() {
    this.#baseURL = "";
    this.#headers = {
      "Content-Type": "application/json",
      Accept: "application/json, text/plain, */*",
    };
    this.#timeout = 5000;
  }

  // Getter & Setter
  setBaseURL(baseUrl: string) {
    this.#baseURL = baseUrl;
  }

  setTimeout(timeout: number) {
    this.#timeout = timeout;
  }

  setHeaders(headers: AxiosRequestHeaders) {
    this.#headers = { ...this.#headers, ...headers };
    this.build();
  }

  setRequestInterceptor(interceptor: any) {
    this.#requestInterceptor = interceptor;
    this.build();
  }

  setResponseInterceptor(interceptor: any) {
    this.#responseInterceptor = interceptor;
    this.build();
  }

  // Builder
  build() {
    this.instance = axios.create({
      baseURL: this.#baseURL,
      headers: this.#headers,
      timeout: this.#timeout,
    });

    if (this.#requestInterceptor) {
      this.instance.interceptors.request.use(
        this.#requestInterceptor!.success,
        this.#requestInterceptor!.error
      );
    } else {
      this.instance.interceptors.response.use(
        function (response: any) {
          if (response.data !== undefined) {
            return response.data;
          }
          return response;
        },
        function (error: any) {
          return Promise.reject(error);
        }
      );
    }

    if (this.#responseInterceptor) {
      this.instance.interceptors.response.use(
        this.#responseInterceptor!.success,
        this.#responseInterceptor!.error
      );
    }
  }

  // Request Processor
  get(url: string) {
    return axios.get(url);
  }

  delete(url: string, config?: any) {
    return axios.delete(url, config);
  }

  post(url: string, data?: any, config?: any) {
    return axios.post(url, data, config);
  }

  put(url: string, data?: any, config?: any) {
    return axios.put(url, data, config);
  }

  patch(url: string, data?: any, config?: any) {
    return axios.patch(url, data, config);
  }
}
