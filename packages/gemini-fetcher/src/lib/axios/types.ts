/*
This is the Standar Types for the AxiosFetcher library
*/

import { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

// The request interceptor must be at least implement this interface
export interface IRequestInterceptor {
  success: (config: AxiosRequestConfig, context: any) => AxiosRequestConfig;
  error: (error: AxiosError, context: any) => Promise<AxiosError>;
}

// The response interceptor must be at least implement this interface
export interface IResponseInterceptor {
  success: (response: AxiosResponse, context: any) => AxiosResponse;
  error: (error: AxiosError, context: any) => Promise<AxiosError>;
}

// Error response standard
// export interface IErrorResponse {
//   config?: any;
//   status: number;
//   data: {
//     error_code: string;
//     attributes?: {
//       blocked_duration: string;
//     };
//     error_metadata?: {
//       callback_url?: string;
//     };
//   };
// }

// The axios creation parameter must be at least implement this interface
export interface ICreation {
  baseURL: string;
  auth?: string;
  requestInterceptor?: IRequestInterceptor;
  responseInterceptor?: IResponseInterceptor;
}
