import axios from "axios";

import { AxiosHelper } from "../index";
import { ICreation } from "../types";

const axiosCreationMock: ICreation = {
  baseURL: "https://virgoku.id/",
  auth: "Bearer token",
  requestInterceptor: {
    success: jest.fn(),
    error: jest.fn(),
  },
  responseInterceptor: {
    success: jest.fn(),
    error: jest.fn(),
  },
};

describe("Axios Tests Groups", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  // 1. Axios Get
  it("1. Axios Get", async () => {
    const axiosMock = AxiosHelper.createAxios(axiosCreationMock);
    axiosMock.setTimeout(3000);
    const mockUrl = "login";
    const trace = jest.spyOn(axios, "get").mockImplementation((url) => {
      return Promise.resolve(axiosCreationMock.baseURL + url);
    });
    const response = await axiosMock.get(mockUrl);
    expect(trace).toHaveBeenCalled();
    expect(response).toBe(axiosCreationMock.baseURL + mockUrl);
  });
  // 2. Axios Delete
  it("2. Axios Delete", async () => {
    const axiosMock = AxiosHelper.createAxios(axiosCreationMock);
    const mockUrl = "login";
    const trace = jest.spyOn(axios, "delete").mockImplementation((url) => {
      return Promise.resolve(axiosCreationMock.baseURL + url);
    });
    const response = await axiosMock.delete(mockUrl);
    expect(trace).toHaveBeenCalled();
    expect(response).toBe(axiosCreationMock.baseURL + mockUrl);
  });
  // 3. Axios Post
  it("3. Axios Post", async () => {
    const axiosMock = AxiosHelper.createAxios(axiosCreationMock);
    const mockUrl = "login";
    const mockData = "data";
    const trace = jest.spyOn(axios, "post").mockImplementation((url, data) => {
      return Promise.resolve({
        url: axiosCreationMock.baseURL + url,
        data,
      });
    });
    const response: any = await axiosMock.post(mockUrl, mockData);
    expect(trace).toHaveBeenCalled();
    expect(response.url).toBe(axiosCreationMock.baseURL + mockUrl);
    expect(response.data).toBe(mockData);
  });
  // 4. Axios Put
  it("4. Axios Put", async () => {
    const axiosMock = AxiosHelper.createAxios(axiosCreationMock);
    const mockUrl = "login";
    const mockData = "data";
    const trace = jest.spyOn(axios, "put").mockImplementation((url, data) => {
      return Promise.resolve({
        url: axiosCreationMock.baseURL + url,
        data,
      });
    });
    const response: any = await axiosMock.put(mockUrl, mockData);
    expect(trace).toHaveBeenCalled();
    expect(response.url).toBe(axiosCreationMock.baseURL + mockUrl);
    expect(response.data).toBe(mockData);
  });
  // 5. Axios Patch
  it("5. Axios Patch", async () => {
    const axiosMock = AxiosHelper.createAxios(axiosCreationMock);
    const mockUrl = "login";
    const mockData = "data";
    const trace = jest.spyOn(axios, "patch").mockImplementation((url, data) => {
      return Promise.resolve({
        url: axiosCreationMock.baseURL + url,
        data,
      });
    });
    const response: any = await axiosMock.patch(mockUrl, mockData);
    expect(trace).toHaveBeenCalled();
    expect(response.url).toBe(axiosCreationMock.baseURL + mockUrl);
    expect(response.data).toBe(mockData);
  });
});
