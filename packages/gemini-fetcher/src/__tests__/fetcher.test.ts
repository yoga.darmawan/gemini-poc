import axios from "axios";

import { FetcherAdapter } from "../index";

describe("Fetcher Tests Groups", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  // 1. Fetcher Get
  it("1. Fetcher Get", async () => {
    const getMock = jest.fn();
    const fetcher = new FetcherAdapter();
    fetcher.setFetcher({
      get: getMock,
    });
    fetcher.setContext(null);
    fetcher.setId("1");
    fetcher.setUrl("1");
    fetcher.setQueryParam({
      id: 1,
    });
    fetcher.setData({
      user_name: "user",
    });
    fetcher.setSuccessHandler(jest.fn());
    fetcher.setErrorHandler(jest.fn());
    fetcher.get();
    expect(getMock).toHaveBeenCalled();
  });
  // 2. Fetcher GetById
  it("2. Fetcher GetById", async () => {
    const getMock = jest.fn();
    const fetcher = new FetcherAdapter();
    fetcher.setFetcher({
      get: getMock,
    });
    fetcher.getById();
    expect(getMock).toHaveBeenCalled();
  });
  // 3. Fetcher Post
  it("3. Fetcher Post", async () => {
    const getMock = jest.fn();
    const fetcher = new FetcherAdapter();
    fetcher.setFetcher({
      post: getMock,
    });
    fetcher.post();
    expect(getMock).toHaveBeenCalled();
  });
  // 4. Fetcher PutById
  it("4. Fetcher PutById", async () => {
    const getMock = jest.fn();
    const fetcher = new FetcherAdapter();
    fetcher.setFetcher({
      put: getMock,
    });
    fetcher.putById();
    expect(getMock).toHaveBeenCalled();
  });
  // 5. Fetcher PatchById
  it("5. Fetcher PatchById", async () => {
    const getMock = jest.fn();
    const fetcher = new FetcherAdapter();
    fetcher.setFetcher({
      patch: getMock,
    });
    fetcher.patchById();
    expect(getMock).toHaveBeenCalled();
  });
  // 6. Fetcher DeleteById
  it("6. Fetcher DeleteById", async () => {
    const getMock = jest.fn();
    const fetcher = new FetcherAdapter();
    fetcher.setFetcher({
      delete: getMock,
    });
    fetcher.deleteById();
    expect(getMock).toHaveBeenCalled();
  });
});
