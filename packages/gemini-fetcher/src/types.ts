// The fetcher must be at least implement this interface
export interface IBaseFetcher {
  get: (url: string) => Promise<any>;
  delete: (url: string, config?: any) => Promise<any>;
  post: (url: string, data?: any, config?: any) => Promise<any>;
  put: (url: string, data?: any, config?: any) => Promise<any>;
  patch: (url: string, data?: any, config?: any) => Promise<any>;
}
