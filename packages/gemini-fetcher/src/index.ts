/*
In this library provided the FetcherAdapter & FetcherHelper
FetcherAdapter is the main function to call your API endpoint. the fetcher library & success/error handler is fully configurable
FetcherHelper is helper to facilitate the fetcher library creation & basic success/error handler with logging feature included
Currently provided only for axios for the library creation, but you can make you own library (axios, fetch, superagent, etc) instance
*/

export { FetcherAdapter } from "./fetcher";
export * as FetcherHelper from "./helper";
