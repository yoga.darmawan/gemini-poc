import React from "react";
import { Button } from "ui";
import { logger } from "@projectaltair/gemini-logger";

export default function Web() {
  React.useEffect(() => {
    const log = logger.console;
    log.info({
      event: "hahaha",
    });
  }, []);
  return (
    <div>
      <h1>Web</h1>
      <Button />
    </div>
  );
}
